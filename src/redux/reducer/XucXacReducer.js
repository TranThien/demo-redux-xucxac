const initialState = {
  mangXucXac: [
    {
      img: "./imgXucXac/1.png",
      number: null,
    },
    {
      img: "./imgXucXac/1.png",
      number: null,
    },
    {
      img: "./imgXucXac/1.png",
      number: null,
    },
  ],
  luachon: null, //(tài (3-11) , xỉu 12-18))
  win: 0,
  numberPlay: 0,
};

export const xucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LUA_CHON":
      {
        state.luachon = action.payload;
      }
      return { ...state };
    case "PLAY_GAME": {
      // tạo ra mảng mới
      let newMangXucXac = state.mangXucXac.map((item) => {
        // random kết quả xúc xắc
        const random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucXac/${random}.png`,
          number: random,
        };
      });
      state.mangXucXac = newMangXucXac;
      // Điểm số các hạt xúc xắc
      let totalSoccer = newMangXucXac.reduce((pre, cur) => {
        return pre + cur.number;
      }, 0);

      // state.mangXucXac = newMangXucXac;
      // số lần chơi ;
      state.numberPlay += 1;
      // tổng số lần thắng=
      if (
        (totalSoccer >= 12 && state.luachon === "XỈU") ||
        (totalSoccer <= 11 && state.luachon === "TÀI")
      ) {
        console.log(state.luachon);
        state.win += 1;
      }
      return { ...state };
    }
    default:
      return state;
  }
};
