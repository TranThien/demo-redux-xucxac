import { combineReducers } from "redux";
import { xucXacReducer } from "./XucXacReducer";

export const rootRenderer_XucXacReducer = combineReducers({
  xucXacReducer: xucXacReducer,
});
