import React, { Component } from "react";
import KetQua from "./KetQua";
import RenderXucXac from "./RenderXucXac";
import "../xucXac.css";

export default class XucXac extends Component {
  render() {
    return (
      <div className="game pt-5">
        <h3 className="text-success mt-3">Game đổ Xúc Xắc </h3>
        <RenderXucXac />
        <KetQua />
      </div>
    );
  }
}
