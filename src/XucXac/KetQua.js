import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    return (
      <div className="mt-5">
        <h2>Bạn chọn : {this.props.luachon} </h2>
        <h3>Số bàn thắng : {this.props.win} </h3>
        <h3>Tổng số bàn chơi : {this.props.numberPlay}</h3>

        <button
          onClick={this.props.handlePlayer}
          className="btn btn-success mt-5 p-3"
          style={{ fontSize: 32 }}
        >
          PLAY GAME
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    luachon: state.xucXacReducer.luachon,
    win: state.xucXacReducer.win,
    numberPlay: state.xucXacReducer.numberPlay,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayer: () => {
      let action = {
        type: "PLAY_GAME",
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
