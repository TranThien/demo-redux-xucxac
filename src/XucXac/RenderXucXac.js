import React, { Component } from "react";
import { connect } from "react-redux";

class RenderXucXac extends Component {
  renderXucXac = () => {
    console.log(this.props.mangXucXac);
    return this.props.mangXucXac.map((item, index) => {
      return (
        <img
          src={item.img}
          alt=""
          style={{ width: 80 }}
          key={index}
          className="mx-3"
        />
      );
    });
  };
  render() {
    return (
      <div className="d-flex justify-content-center align-items-center">
        <button
          onClick={() => {
            this.props.handleChangeSelect("TÀI");
          }}
          className="btn btn-success p-3"
          style={{ fontSize: 24 }}
        >
          TÀI
        </button>
        <div className="mx-2">{this.renderXucXac()}</div>
        <button
          onClick={() => {
            this.props.handleChangeSelect("XỈU");
          }}
          className="btn btn-success p-3"
          style={{ fontSize: 24 }}
        >
          {" "}
          XỈU
        </button>
      </div>
    );
  }
}

// lấy dữ liệu từ redux
let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};
// gửi dữ liệu lên cho redux xử lí
let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeSelect: (taiXiu) => {
      const action = {
        type: "LUA_CHON",
        payload: taiXiu,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RenderXucXac);
