import logo from "./logo.svg";
import "./App.css";
import XucXac from "./XucXac/XucXac";

function App() {
  return (
    <div className="App">
      <XucXac />
    </div>
  );
}

export default App;
